package JAXB;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Xml2URJC {
	public static void main(String[] args) {

		try {

			File file = new File("masters");
			JAXBContext jaxbContext = JAXBContext.newInstance(UniversidadReyJuanCarlos.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			UniversidadReyJuanCarlos urjc = (UniversidadReyJuanCarlos) jaxbUnmarshaller.unmarshal(file);

			System.out.println(urjc.getRector() + " " + urjc.getMasters().get(0).getNombre());

			/*
			 * Alumne dani = new Alumne(201,"Dani","Suñe","2DAM",5,1.71);
			 * 
			 * m6.setProfessor("Nicolas Torrubiano"); m6.getAls().add(dani);
			 * 
			 * Marshaller marshallerObj = jaxbContext.createMarshaller();
			 * marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			 * 
			 * marshallerObj.marshal(m6, new FileOutputStream("modul-new"));
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
